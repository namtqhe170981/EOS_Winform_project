﻿Create table Exam(
	[Code] varchar(50) primary key,
	[Time] int,
	[TotalMark] int,
	[SubjectName] varchar(10)
)

Create table Student(
	[Account] varchar(30) primary key,
	[Password] varchar(30),
	[Name] nvarchar(30) default null
)

Create table Question(
	[Code] int not null identity(1, 1) primary key,
	[Title] varchar(max),
	[ExamCode] varchar(50),
	[Image] varchar(max) default null,
	foreign key ([ExamCode]) references Exam(Code)
)

Create table Answer(
	[Order] int not null,
	[QuestionCode] int,
	[Title] varchar(max),
	[IsCorrect] bit,
	primary key ([Order],[QuestionCode]),
	foreign key ([QuestionCode]) references Question(Code)
)

Create table Mark(
	[ExamCode] varchar(50),
	[StudentAccount] varchar(30),
	[Mark] float default 0,
	primary key ([ExamCode],[StudentAccount]),
	foreign key ([StudentAccount]) references Student(Account),
	foreign key ([ExamCode]) references Exam(Code)
)

Create table Mark2(
	[ExamCode] varchar(50),
	[StudentAccount] varchar(30),
	[Mark] float default 0,
	[order] int not null,
	primary key ([ExamCode],[StudentAccount]),
	foreign key ([StudentAccount]) references Student(Account),
	foreign key ([ExamCode]) references Exam(Code)
)

Create table AssignRequest(
	[ExamCode] varchar(50),
	[StudentAccount] varchar(30),
	primary key ([ExamCode],[StudentAccount]),
	foreign key ([StudentAccount]) references Student(Account),
	foreign key ([ExamCode]) references Exam(Code)
)


select * from student
insert into Student(Account, [Password])
	values ('namtqhe170981','017267606')

select * from exam

insert into Exam(Code, [Time], TotalMark, SubjectName)
	values ('1','1','5','DEMO')

select * from Question where ExamCode = 1

select * from Answer where QuestionCode = 5

insert into Question([Title], [ExamCode])
	values ('The endorsement of which of the following types of stakeholder is most critical when developing mission and vision statements?',1)

insert into Answer([Order], [QuestionCode], [Title], [IsCorrect])
	values (1, 6,'A. Employees',0),
	(2, 6,'B. Customers',0),
	(3, 6,'C. C-suite executives', 1),
	(4, 5,'D. Department heads', 0),
	(5, 5,'E. Mid-Sprint Status Review Meeting', 0)

select * from Question where ExamCode = 'MAS291_Test1_Sp23_972423'
select a.* from Answer a join Question q on  a.QuestionCode = q.Code where 
	q.ExamCode = 'MAS291_Test1_Sp23_972423' and a.IsCorrect = 1

select * from Exam
select * from mark
delete from mark

insert into Question([Title], [ExamCode])
	values ('The fraction of defective integrated circuits produced in a photolithography process is being studied. A random sample of 500 circuits is tested, revealing 20 defectives. Calculate a 95% upper confidence bound on the fraction of defective circuits.
Let z(0.025) =1.96; z(0.05) =1.65.','MAS291_Test1_Sp23_972423')

insert into Answer([Order], [QuestionCode], [Title], [IsCorrect]) values 
	(1, 21,'A. Observational Study is a basic method of presentation data', 0),
	(2, 21,'B. Observational Study is a basic method of analysis data', 0),
	(3, 21,'C. None of these', 0),
	(4, 21,'D. Observational Study is a basic method of collecting data', 1),
	(5, 19,'E. None of the other choices is correct', 0)
