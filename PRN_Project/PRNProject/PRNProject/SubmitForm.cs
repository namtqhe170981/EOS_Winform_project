﻿using PRNProject.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRNProject
{
    public partial class SubmitForm : Form
    {
        public SubmitForm()
        {
            InitializeComponent();
            this.Text = "EOS Submit";
        }

        public SubmitForm(string code, string name, string currentTimeLeft)
        {
            InitializeComponent();
            this.Text = "EOS Submit";
            using (WinformProjectContext context = new WinformProjectContext())
            {
                Student s = context.Students.FirstOrDefault(s => s.Account == name);
                lblDisplayStudent.Text = name;
                Exam e = context.Exams.FirstOrDefault(e => e.Code == code);
                lblDisplayExamCode.Text = e.SubjectName;
                lblDisplayDuration.Text = e.Time + " minutes";
                lblDisplayTotalMarks.Text = e.TotalMark + "";
                Mark mark = context.Marks.FirstOrDefault(m => m.ExamCode == code && m.StudentAccount == name);
                lblMark.Text = "Your mark is " + mark.Mark1
                + "\nThe grade will be stored in the database.";
            }
            lblDisplayTimeLeft.Text = currentTimeLeft;
        }

        public SubmitForm(string code, string name, string currentTimeLeft, string mark)
        {
            InitializeComponent();
            this.Text = "EOS Submit";
            using (WinformProjectContext context = new WinformProjectContext())
            {
                Student s = context.Students.FirstOrDefault(s => s.Account == name);
                lblDisplayStudent.Text = name;
                Exam e = context.Exams.FirstOrDefault(e => e.Code == code);
                lblDisplayExamCode.Text = e.SubjectName;
                lblDisplayDuration.Text = e.Time + " minutes";
                lblDisplayTotalMarks.Text = e.TotalMark + "";
            }
            lblDisplayTimeLeft.Text = currentTimeLeft;
            lblMark.Text = "Your mark is " + mark
                + "\nThe grade will be stored in the database.";
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void SubmitForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
