﻿using PRNProject.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRNProject
{
    public partial class RequestAssigned : Form
    {
        public RequestAssigned()
        {
            InitializeComponent();
        }
        public RequestAssigned(string examCode, string studentCode)
        {
            InitializeComponent();
            lblReport.Text = $"{studentCode} already taken the exam {examCode}!\n " +
                $"Do you want to retake this exam?";
            exam = examCode;
            code = studentCode;
        }
        string exam = "";
        string code = "";
        private void btnNo_Click(object sender, EventArgs e)
        {
            this.Hide();
            LoginForm l = new LoginForm();
            l.Show();
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            using (WinformProjectContext c = new WinformProjectContext())
            {
                Mark m = c.Marks.FirstOrDefault(m => m.ExamCode == exam && m.StudentAccount == code);
                if (m == null)
                {
                    Mark m1 = new Mark();
                    m1.Mark1 = 0;
                    m1.ExamCode = exam;
                    m1.StudentAccount = code;
                    c.Marks.Add(m1);
                    c.SaveChanges();
                }
                MessageBox.Show("Request been taken!");
                Environment.Exit(0);
            }
                
        }
    }
}
