﻿using PRNProject.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRNProject
{
    public partial class adminLogin : Form
    {
        public adminLogin()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string acc = txtAcc.Text;
            string pass = txtPass.Text;
            if (acc == "" || pass == "")
            {
                MessageBox.Show("Some input is left empty!");
            }
            else
            {
                using (WinformProjectContext context = new WinformProjectContext())
                {
                    Student user = context.Students.FirstOrDefault(u => u.Account == acc && u.Password == pass);

                    if (user != null)
                    {
                        // Đăng nhập thành công
                        this.Hide();
                        ResponseAssigned r = new ResponseAssigned();
                        r.Show();
                    }
                    else
                    {
                        // Đăng nhập thất bại
                        MessageBox.Show("The account or password is incorrect!");
                    }
                }
            }
        }
    }
}
