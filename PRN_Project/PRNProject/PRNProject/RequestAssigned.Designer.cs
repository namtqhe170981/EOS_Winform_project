﻿namespace PRNProject
{
    partial class RequestAssigned
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            lblReport = new Label();
            btnYes = new Button();
            btnNo = new Button();
            SuspendLayout();
            // 
            // lblReport
            // 
            lblReport.AutoSize = true;
            lblReport.Location = new Point(265, 173);
            lblReport.Name = "lblReport";
            lblReport.Size = new Size(283, 40);
            lblReport.TabIndex = 0;
            lblReport.Text = "You already taken this exam!\r\nDo you want to request retake this exam?\r\n";
            // 
            // btnYes
            // 
            btnYes.Location = new Point(195, 271);
            btnYes.Name = "btnYes";
            btnYes.Size = new Size(94, 29);
            btnYes.TabIndex = 1;
            btnYes.Text = "Yes";
            btnYes.UseVisualStyleBackColor = true;
            btnYes.Click += btnYes_Click;
            // 
            // btnNo
            // 
            btnNo.Location = new Point(509, 273);
            btnNo.Name = "btnNo";
            btnNo.Size = new Size(94, 29);
            btnNo.TabIndex = 2;
            btnNo.Text = "No";
            btnNo.UseVisualStyleBackColor = true;
            btnNo.Click += btnNo_Click;
            // 
            // RequestAssigned
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(btnNo);
            Controls.Add(btnYes);
            Controls.Add(lblReport);
            Name = "RequestAssigned";
            Text = "RequestAssigned";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label lblReport;
        private Button btnYes;
        private Button btnNo;
    }
}