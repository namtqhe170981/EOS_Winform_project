﻿using System;
using System.Collections.Generic;

namespace PRNProject.Models
{
    public partial class Answer
    {
        public int Order { get; set; }
        public int QuestionCode { get; set; }
        public string? Title { get; set; }
        public bool? IsCorrect { get; set; }

        public virtual Question QuestionCodeNavigation { get; set; } = null!;
    }
}
