﻿using System;
using System.Collections.Generic;

namespace PRNProject.Models
{
    public partial class Mark
    {
        public string ExamCode { get; set; } = null!;
        public string StudentAccount { get; set; } = null!;
        public double? Mark1 { get; set; }

        public virtual Exam ExamCodeNavigation { get; set; } = null!;
        public virtual Student StudentAccountNavigation { get; set; } = null!;
    }
}
