﻿using System;
using System.Collections.Generic;

namespace PRNProject.Models
{
    public partial class Question
    {
        public Question()
        {
            Answers = new HashSet<Answer>();
        }

        public int Code { get; set; }
        public string? Title { get; set; }
        public string? ExamCode { get; set; }
        public string? Image { get; set; }

        public virtual Exam? ExamCodeNavigation { get; set; }
        public virtual ICollection<Answer> Answers { get; set; }
    }
}
