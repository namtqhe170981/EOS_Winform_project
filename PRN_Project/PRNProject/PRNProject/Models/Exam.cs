﻿using System;
using System.Collections.Generic;

namespace PRNProject.Models
{
    public partial class Exam
    {
        public Exam()
        {
            Marks = new HashSet<Mark>();
            Questions = new HashSet<Question>();
            StudentAccounts = new HashSet<Student>();
        }

        public string Code { get; set; } = null!;
        public int? Time { get; set; }
        public int? TotalMark { get; set; }
        public string? SubjectName { get; set; }

        public virtual ICollection<Mark> Marks { get; set; }
        public virtual ICollection<Question> Questions { get; set; }

        public virtual ICollection<Student> StudentAccounts { get; set; }
    }
}
