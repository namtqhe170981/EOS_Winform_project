﻿using System;
using System.Collections.Generic;

namespace PRNProject.Models
{
    public partial class Student
    {
        public Student()
        {
            Marks = new HashSet<Mark>();
            ExamCodes = new HashSet<Exam>();
        }

        public string Account { get; set; } = null!;
        public string? Password { get; set; }
        public string? Name { get; set; }

        public virtual ICollection<Mark> Marks { get; set; }

        public virtual ICollection<Exam> ExamCodes { get; set; }
    }
}
