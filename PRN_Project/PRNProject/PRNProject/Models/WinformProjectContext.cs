﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace PRNProject.Models
{
    public partial class WinformProjectContext : DbContext
    {
        public WinformProjectContext()
        {
        }

        public WinformProjectContext(DbContextOptions<WinformProjectContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Answer> Answers { get; set; } = null!;
        public virtual DbSet<Exam> Exams { get; set; } = null!;
        public virtual DbSet<Mark> Marks { get; set; } = null!;
        public virtual DbSet<Question> Questions { get; set; } = null!;
        public virtual DbSet<Student> Students { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("server=DESKTOP-4K0CTVJ;database=WinformProject;uid=sa;pwd=12345;TrustServerCertificate=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Answer>(entity =>
            {
                entity.HasKey(e => new { e.Order, e.QuestionCode })
                    .HasName("PK__Answer__D01616AA0E5B9EB6");

                entity.ToTable("Answer");

                entity.Property(e => e.Title).IsUnicode(false);

                entity.HasOne(d => d.QuestionCodeNavigation)
                    .WithMany(p => p.Answers)
                    .HasForeignKey(d => d.QuestionCode)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Answer__Question__44FF419A");
            });

            modelBuilder.Entity<Exam>(entity =>
            {
                entity.HasKey(e => e.Code)
                    .HasName("PK__Exam__A25C5AA63D362C94");

                entity.ToTable("Exam");

                entity.Property(e => e.Code)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SubjectName)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasMany(d => d.StudentAccounts)
                    .WithMany(p => p.ExamCodes)
                    .UsingEntity<Dictionary<string, object>>(
                        "AssignRequest",
                        l => l.HasOne<Student>().WithMany().HasForeignKey("StudentAccount").OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK__AssignReq__Stude__6FE99F9F"),
                        r => r.HasOne<Exam>().WithMany().HasForeignKey("ExamCode").OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK__AssignReq__ExamC__70DDC3D8"),
                        j =>
                        {
                            j.HasKey("ExamCode", "StudentAccount").HasName("PK__AssignRe__3D3E53F71EE4D202");

                            j.ToTable("AssignRequest");

                            j.IndexerProperty<string>("ExamCode").HasMaxLength(50).IsUnicode(false);

                            j.IndexerProperty<string>("StudentAccount").HasMaxLength(30).IsUnicode(false);
                        });
            });

            modelBuilder.Entity<Mark>(entity =>
            {
                entity.HasKey(e => new { e.ExamCode, e.StudentAccount })
                    .HasName("PK__Mark__3D3E53F7921CE3D4");

                entity.ToTable("Mark");

                entity.Property(e => e.ExamCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StudentAccount)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Mark1)
                    .HasColumnName("Mark")
                    .HasDefaultValueSql("((0))");

                entity.HasOne(d => d.ExamCodeNavigation)
                    .WithMany(p => p.Marks)
                    .HasForeignKey(d => d.ExamCode)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Mark__ExamCode__49C3F6B7");

                entity.HasOne(d => d.StudentAccountNavigation)
                    .WithMany(p => p.Marks)
                    .HasForeignKey(d => d.StudentAccount)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Mark__StudentAcc__48CFD27E");
            });

            modelBuilder.Entity<Question>(entity =>
            {
                entity.HasKey(e => e.Code)
                    .HasName("PK__Question__A25C5AA659FCD333");

                entity.ToTable("Question");

                entity.Property(e => e.ExamCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Image).IsUnicode(false);

                entity.Property(e => e.Title).IsUnicode(false);

                entity.HasOne(d => d.ExamCodeNavigation)
                    .WithMany(p => p.Questions)
                    .HasForeignKey(d => d.ExamCode)
                    .HasConstraintName("FK__Question__ExamCo__4222D4EF");
            });

            modelBuilder.Entity<Student>(entity =>
            {
                entity.HasKey(e => e.Account)
                    .HasName("PK__Student__B0C3AC4785955609");

                entity.ToTable("Student");

                entity.Property(e => e.Account)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Name).HasMaxLength(30);

                entity.Property(e => e.Password)
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
