﻿using Microsoft.Data.SqlClient;
using PRNProject.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.StartPanel;

namespace PRNProject
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
            this.Text = "EOS Login";
            txtDomain.Text = "FU.EDU.VN";
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void LoginForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string examCode = txtExam.Text;
            string acc = txtAccount.Text;
            string pass = txtPass.Text;
            if (examCode == "" || acc == "" || pass == "")
            {
                MessageBox.Show("Some input is left empty!");
            }
            else
            {
                using (WinformProjectContext context = new WinformProjectContext())
                {
                    var exam = context.Exams.FirstOrDefault(e => e.Code == examCode);
                    if (exam != null)
                    {
                        Student user = context.Students.FirstOrDefault(u => u.Account == acc && u.Password == pass);

                        if (user != null)
                        {
                            Mark mark = context.Marks.FirstOrDefault(m => m.ExamCode == examCode && m.StudentAccount == acc);
                            if (mark == null)
                            {
                                // Đăng nhập thành công
                                this.Hide();
                                DoTheTestForm doTheTestForm = new DoTheTestForm(examCode, acc);
                                doTheTestForm.Show();
                            }
                            else
                            {
                                //MessageBox.Show("You have already taken this exam!");
                                this.Hide();
                                RequestAssigned r = new RequestAssigned(examCode, acc);
                                r.Show();
                            }
                        }
                        else
                        {
                            // Đăng nhập thất bại
                            MessageBox.Show("The account or password is incorrect!");
                        }
                    }
                    else
                    {
                        MessageBox.Show("The code exam is not exist!");
                    }
                }
            }
        }
    }
}
