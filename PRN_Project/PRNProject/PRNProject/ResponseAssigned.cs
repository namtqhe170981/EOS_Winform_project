﻿using PRNProject.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRNProject
{
    public partial class ResponseAssigned : Form
    {
        public ResponseAssigned()
        {
            InitializeComponent();
        }

        private void ResponseAssigned_Load(object sender, EventArgs e)
        {
            using (WinformProjectContext ctx = new WinformProjectContext())
            {
                List<Mark> marks = ctx.Marks.ToList();
                dtReport.DataSource = null;

                dtReport.DataSource = marks;
            }

        }

        private void btnApprove_Click(object sender, EventArgs e)
        {
            using (WinformProjectContext ctx = new WinformProjectContext())
            {

                int selectedRowIndex = dtReport.CurrentRow.Index;

                string examCode = dtReport.Rows[selectedRowIndex].Cells["ExamCode"].Value.ToString();
                string studentName = dtReport.Rows[selectedRowIndex].Cells["StudentAccount"].Value.ToString();

                // Sử dụng giá trị của hàng được chọn ở đây
                Mark m = ctx.Marks.FirstOrDefault(ma => ma.ExamCode == examCode &&
                ma.StudentAccount == studentName);
                ctx.Marks.Remove(m);
                ctx.SaveChanges();
                MessageBox.Show("Accept the response!");
                ResponseAssigned_Load(sender, e);

            }
        }

        private void dtReport_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
