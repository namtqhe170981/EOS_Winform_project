﻿namespace PRNProject
{
    partial class DoTheTestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            lblDuration = new Label();
            lblTotalMark = new Label();
            lblStudent = new Label();
            lblExamCode = new Label();
            lblDisplayExamCode = new Label();
            lblDisplayDuration = new Label();
            lblDisplayTotalMarks = new Label();
            lblDisplayStudent = new Label();
            lblTime = new Label();
            tmTimeLeft = new System.Windows.Forms.Timer(components);
            lblDisplayTimeLeft = new Label();
            chkFinish = new CheckBox();
            btnFinish = new Button();
            btnExit = new Button();
            lblReportTotalMark = new Label();
            lblCheckBoxAnswer = new Label();
            lblReportTypeExam = new Label();
            pgbProcess = new ProgressBar();
            btnBack = new Button();
            btnNext = new Button();
            pnRed = new Panel();
            lblShowChooseAnswer = new Label();
            lblShowQuestion = new Label();
            pnAnswers = new Panel();
            lblFont = new Label();
            lblSize = new Label();
            numSize = new NumericUpDown();
            domFont = new DomainUpDown();
            ((System.ComponentModel.ISupportInitialize)numSize).BeginInit();
            SuspendLayout();
            // 
            // lblDuration
            // 
            lblDuration.AutoSize = true;
            lblDuration.Location = new Point(56, 19);
            lblDuration.Name = "lblDuration";
            lblDuration.Size = new Size(70, 20);
            lblDuration.TabIndex = 0;
            lblDuration.Text = "Duration:";
            // 
            // lblTotalMark
            // 
            lblTotalMark.AutoSize = true;
            lblTotalMark.Location = new Point(12, 65);
            lblTotalMark.Name = "lblTotalMark";
            lblTotalMark.Size = new Size(114, 20);
            lblTotalMark.TabIndex = 1;
            lblTotalMark.Text = "Total Questions:";
            // 
            // lblStudent
            // 
            lblStudent.AutoSize = true;
            lblStudent.Location = new Point(315, 20);
            lblStudent.Name = "lblStudent";
            lblStudent.Size = new Size(63, 20);
            lblStudent.TabIndex = 2;
            lblStudent.Text = "Student:";
            // 
            // lblExamCode
            // 
            lblExamCode.AutoSize = true;
            lblExamCode.Location = new Point(291, 65);
            lblExamCode.Name = "lblExamCode";
            lblExamCode.Size = new Size(87, 20);
            lblExamCode.TabIndex = 3;
            lblExamCode.Text = "Exam Code:";
            // 
            // lblDisplayExamCode
            // 
            lblDisplayExamCode.AutoSize = true;
            lblDisplayExamCode.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            lblDisplayExamCode.ForeColor = Color.Blue;
            lblDisplayExamCode.Location = new Point(404, 58);
            lblDisplayExamCode.Name = "lblDisplayExamCode";
            lblDisplayExamCode.Size = new Size(24, 28);
            lblDisplayExamCode.TabIndex = 4;
            lblDisplayExamCode.Text = "4";
            // 
            // lblDisplayDuration
            // 
            lblDisplayDuration.AutoSize = true;
            lblDisplayDuration.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            lblDisplayDuration.ForeColor = Color.Blue;
            lblDisplayDuration.Location = new Point(140, 12);
            lblDisplayDuration.Name = "lblDisplayDuration";
            lblDisplayDuration.Size = new Size(24, 28);
            lblDisplayDuration.TabIndex = 5;
            lblDisplayDuration.Text = "1";
            // 
            // lblDisplayTotalMarks
            // 
            lblDisplayTotalMarks.AutoSize = true;
            lblDisplayTotalMarks.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            lblDisplayTotalMarks.ForeColor = Color.Blue;
            lblDisplayTotalMarks.Location = new Point(140, 60);
            lblDisplayTotalMarks.Name = "lblDisplayTotalMarks";
            lblDisplayTotalMarks.Size = new Size(24, 28);
            lblDisplayTotalMarks.TabIndex = 6;
            lblDisplayTotalMarks.Text = "2";
            // 
            // lblDisplayStudent
            // 
            lblDisplayStudent.AutoSize = true;
            lblDisplayStudent.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            lblDisplayStudent.ForeColor = Color.Blue;
            lblDisplayStudent.Location = new Point(404, 13);
            lblDisplayStudent.Name = "lblDisplayStudent";
            lblDisplayStudent.Size = new Size(24, 28);
            lblDisplayStudent.TabIndex = 7;
            lblDisplayStudent.Text = "3";
            // 
            // lblTime
            // 
            lblTime.AutoSize = true;
            lblTime.Location = new Point(825, 65);
            lblTime.Name = "lblTime";
            lblTime.Size = new Size(71, 20);
            lblTime.TabIndex = 8;
            lblTime.Text = "Time left:";
            // 
            // tmTimeLeft
            // 
            tmTimeLeft.Tick += tmTimeLeft_Tick;
            // 
            // lblDisplayTimeLeft
            // 
            lblDisplayTimeLeft.AutoSize = true;
            lblDisplayTimeLeft.Font = new Font("Segoe UI", 36F, FontStyle.Bold, GraphicsUnit.Point);
            lblDisplayTimeLeft.ForeColor = Color.Blue;
            lblDisplayTimeLeft.Location = new Point(921, 19);
            lblDisplayTimeLeft.Name = "lblDisplayTimeLeft";
            lblDisplayTimeLeft.Size = new Size(161, 81);
            lblDisplayTimeLeft.TabIndex = 9;
            lblDisplayTimeLeft.Text = "NaN";
            // 
            // chkFinish
            // 
            chkFinish.AutoSize = true;
            chkFinish.ForeColor = Color.Blue;
            chkFinish.Location = new Point(7, 664);
            chkFinish.Name = "chkFinish";
            chkFinish.Size = new Size(193, 24);
            chkFinish.TabIndex = 10;
            chkFinish.Text = "I want to finish the exam";
            chkFinish.UseVisualStyleBackColor = true;
            chkFinish.CheckedChanged += chkFinish_CheckedChanged;
            // 
            // btnFinish
            // 
            btnFinish.Enabled = false;
            btnFinish.Location = new Point(7, 697);
            btnFinish.Name = "btnFinish";
            btnFinish.Size = new Size(94, 29);
            btnFinish.TabIndex = 11;
            btnFinish.Text = "Finish";
            btnFinish.UseVisualStyleBackColor = true;
            btnFinish.Click += btnFinish_Click;
            // 
            // btnExit
            // 
            btnExit.Enabled = false;
            btnExit.Location = new Point(1173, 697);
            btnExit.Name = "btnExit";
            btnExit.Size = new Size(94, 29);
            btnExit.TabIndex = 12;
            btnExit.Text = "Exit";
            btnExit.UseVisualStyleBackColor = true;
            // 
            // lblReportTotalMark
            // 
            lblReportTotalMark.AutoSize = true;
            lblReportTotalMark.Font = new Font("Segoe UI", 10.8F, FontStyle.Bold, GraphicsUnit.Point);
            lblReportTotalMark.ForeColor = Color.Blue;
            lblReportTotalMark.Location = new Point(7, 174);
            lblReportTotalMark.Name = "lblReportTotalMark";
            lblReportTotalMark.Size = new Size(187, 25);
            lblReportTotalMark.TabIndex = 13;
            lblReportTotalMark.Text = "Describe the process";
            // 
            // lblCheckBoxAnswer
            // 
            lblCheckBoxAnswer.AutoSize = true;
            lblCheckBoxAnswer.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            lblCheckBoxAnswer.ForeColor = Color.Blue;
            lblCheckBoxAnswer.Location = new Point(37, 199);
            lblCheckBoxAnswer.Name = "lblCheckBoxAnswer";
            lblCheckBoxAnswer.Size = new Size(69, 23);
            lblCheckBoxAnswer.TabIndex = 14;
            lblCheckBoxAnswer.Text = "Answer";
            // 
            // lblReportTypeExam
            // 
            lblReportTypeExam.AutoSize = true;
            lblReportTypeExam.BackColor = Color.White;
            lblReportTypeExam.Location = new Point(7, 135);
            lblReportTypeExam.Name = "lblReportTypeExam";
            lblReportTypeExam.Size = new Size(119, 20);
            lblReportTypeExam.TabIndex = 15;
            lblReportTypeExam.Text = "Multiple Choices";
            // 
            // pgbProcess
            // 
            pgbProcess.Location = new Point(505, 174);
            pgbProcess.Name = "pgbProcess";
            pgbProcess.Size = new Size(762, 29);
            pgbProcess.TabIndex = 16;
            // 
            // btnBack
            // 
            btnBack.Location = new Point(7, 593);
            btnBack.Name = "btnBack";
            btnBack.Size = new Size(53, 29);
            btnBack.TabIndex = 17;
            btnBack.Text = "Back";
            btnBack.UseVisualStyleBackColor = true;
            btnBack.Click += btnBack_Click;
            // 
            // btnNext
            // 
            btnNext.Location = new Point(72, 593);
            btnNext.Name = "btnNext";
            btnNext.Size = new Size(50, 29);
            btnNext.TabIndex = 18;
            btnNext.Text = "Next";
            btnNext.UseVisualStyleBackColor = true;
            btnNext.Click += btnNext_Click;
            // 
            // pnRed
            // 
            pnRed.BackColor = Color.Red;
            pnRed.Font = new Font("Segoe UI", 7.8F, FontStyle.Regular, GraphicsUnit.Point);
            pnRed.Location = new Point(567, 215);
            pnRed.Margin = new Padding(0);
            pnRed.Name = "pnRed";
            pnRed.Size = new Size(10, 407);
            pnRed.TabIndex = 19;
            // 
            // lblShowChooseAnswer
            // 
            lblShowChooseAnswer.BackColor = Color.White;
            lblShowChooseAnswer.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            lblShowChooseAnswer.Location = new Point(161, 215);
            lblShowChooseAnswer.Name = "lblShowChooseAnswer";
            lblShowChooseAnswer.Size = new Size(393, 407);
            lblShowChooseAnswer.TabIndex = 20;
            lblShowChooseAnswer.Text = "(Choose n answer)";
            // 
            // lblShowQuestion
            // 
            lblShowQuestion.BackColor = Color.White;
            lblShowQuestion.Location = new Point(583, 216);
            lblShowQuestion.Name = "lblShowQuestion";
            lblShowQuestion.Size = new Size(684, 406);
            lblShowQuestion.TabIndex = 21;
            lblShowQuestion.Text = "Content here";
            // 
            // pnAnswers
            // 
            pnAnswers.Location = new Point(37, 239);
            pnAnswers.Name = "pnAnswers";
            pnAnswers.Size = new Size(61, 348);
            pnAnswers.TabIndex = 22;
            // 
            // lblFont
            // 
            lblFont.AutoSize = true;
            lblFont.Location = new Point(337, 105);
            lblFont.Name = "lblFont";
            lblFont.Size = new Size(41, 20);
            lblFont.TabIndex = 23;
            lblFont.Text = "Font:";
            // 
            // lblSize
            // 
            lblSize.AutoSize = true;
            lblSize.Location = new Point(87, 105);
            lblSize.Name = "lblSize";
            lblSize.Size = new Size(39, 20);
            lblSize.TabIndex = 24;
            lblSize.Text = "Size:";
            // 
            // numSize
            // 
            numSize.Location = new Point(140, 98);
            numSize.Maximum = new decimal(new int[] { 14, 0, 0, 0 });
            numSize.Minimum = new decimal(new int[] { 8, 0, 0, 0 });
            numSize.Name = "numSize";
            numSize.ReadOnly = true;
            numSize.Size = new Size(66, 27);
            numSize.TabIndex = 26;
            numSize.Value = new decimal(new int[] { 9, 0, 0, 0 });
            numSize.ValueChanged += numSize_ValueChanged;
            // 
            // domFont
            // 
            domFont.Items.Add("Segoe UI");
            domFont.Items.Add("Microsoft Sans Serif");
            domFont.Items.Add("Times New Roman");
            domFont.Location = new Point(404, 98);
            domFont.Name = "domFont";
            domFont.ReadOnly = true;
            domFont.Size = new Size(266, 27);
            domFont.TabIndex = 27;
            domFont.Text = "Segue UI";
            domFont.SelectedItemChanged += domFont_SelectedItemChanged;
            // 
            // DoTheTestForm
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1279, 741);
            Controls.Add(domFont);
            Controls.Add(numSize);
            Controls.Add(lblSize);
            Controls.Add(lblFont);
            Controls.Add(pnAnswers);
            Controls.Add(lblShowQuestion);
            Controls.Add(lblShowChooseAnswer);
            Controls.Add(pnRed);
            Controls.Add(btnNext);
            Controls.Add(btnBack);
            Controls.Add(pgbProcess);
            Controls.Add(lblReportTypeExam);
            Controls.Add(lblCheckBoxAnswer);
            Controls.Add(lblReportTotalMark);
            Controls.Add(btnExit);
            Controls.Add(btnFinish);
            Controls.Add(chkFinish);
            Controls.Add(lblDisplayTimeLeft);
            Controls.Add(lblTime);
            Controls.Add(lblDisplayStudent);
            Controls.Add(lblDisplayTotalMarks);
            Controls.Add(lblDisplayDuration);
            Controls.Add(lblDisplayExamCode);
            Controls.Add(lblExamCode);
            Controls.Add(lblStudent);
            Controls.Add(lblTotalMark);
            Controls.Add(lblDuration);
            Name = "DoTheTestForm";
            Text = "DoTheTestForm";
            FormClosing += DoTheTestForm_FormClosing;
            ((System.ComponentModel.ISupportInitialize)numSize).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label lblDuration;
        private Label lblTotalMark;
        private Label lblStudent;
        private Label lblExamCode;
        private Label lblDisplayExamCode;
        private Label lblDisplayDuration;
        private Label lblDisplayTotalMarks;
        private Label lblDisplayStudent;
        private Label lblTime;
        private System.Windows.Forms.Timer tmTimeLeft;
        private Label lblDisplayTimeLeft;
        private CheckBox chkFinish;
        private Button btnFinish;
        private Button btnExit;
        private Label lblReportTotalMark;
        private Label lblCheckBoxAnswer;
        private Label lblReportTypeExam;
        private ProgressBar pgbProcess;
        private Button btnBack;
        private Button btnNext;
        private Panel pnRed;
        private Label lblShowChooseAnswer;
        private Label lblShowQuestion;
        private Panel pnAnswers;
        private Label lblFont;
        private Label lblSize;
        private NumericUpDown numSize;
        private DomainUpDown domFont;
    }
}