﻿namespace PRNProject
{
    partial class SubmitForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            btnExit = new Button();
            lblDisplayStudent = new Label();
            lblDisplayTotalMarks = new Label();
            lblDisplayDuration = new Label();
            lblDisplayExamCode = new Label();
            lblExamCode = new Label();
            lblStudent = new Label();
            lblTotalMark = new Label();
            lblDuration = new Label();
            lblDisplayTimeLeft = new Label();
            lblTime = new Label();
            btnFinish = new Button();
            chkFinish = new CheckBox();
            lblMark = new Label();
            lblSubmitSuccess = new Label();
            SuspendLayout();
            // 
            // btnExit
            // 
            btnExit.Location = new Point(871, 511);
            btnExit.Name = "btnExit";
            btnExit.Size = new Size(94, 29);
            btnExit.TabIndex = 0;
            btnExit.Text = "Exit";
            btnExit.UseVisualStyleBackColor = true;
            btnExit.Click += btnExit_Click;
            // 
            // lblDisplayStudent
            // 
            lblDisplayStudent.AutoSize = true;
            lblDisplayStudent.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            lblDisplayStudent.ForeColor = Color.Blue;
            lblDisplayStudent.Location = new Point(391, 15);
            lblDisplayStudent.Name = "lblDisplayStudent";
            lblDisplayStudent.Size = new Size(24, 28);
            lblDisplayStudent.TabIndex = 15;
            lblDisplayStudent.Text = "3";
            // 
            // lblDisplayTotalMarks
            // 
            lblDisplayTotalMarks.AutoSize = true;
            lblDisplayTotalMarks.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            lblDisplayTotalMarks.ForeColor = Color.Blue;
            lblDisplayTotalMarks.Location = new Point(147, 61);
            lblDisplayTotalMarks.Name = "lblDisplayTotalMarks";
            lblDisplayTotalMarks.Size = new Size(24, 28);
            lblDisplayTotalMarks.TabIndex = 14;
            lblDisplayTotalMarks.Text = "2";
            // 
            // lblDisplayDuration
            // 
            lblDisplayDuration.AutoSize = true;
            lblDisplayDuration.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            lblDisplayDuration.ForeColor = Color.Blue;
            lblDisplayDuration.Location = new Point(147, 15);
            lblDisplayDuration.Name = "lblDisplayDuration";
            lblDisplayDuration.Size = new Size(24, 28);
            lblDisplayDuration.TabIndex = 13;
            lblDisplayDuration.Text = "1";
            // 
            // lblDisplayExamCode
            // 
            lblDisplayExamCode.AutoSize = true;
            lblDisplayExamCode.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            lblDisplayExamCode.ForeColor = Color.Blue;
            lblDisplayExamCode.Location = new Point(391, 60);
            lblDisplayExamCode.Name = "lblDisplayExamCode";
            lblDisplayExamCode.Size = new Size(24, 28);
            lblDisplayExamCode.TabIndex = 12;
            lblDisplayExamCode.Text = "4";
            // 
            // lblExamCode
            // 
            lblExamCode.AutoSize = true;
            lblExamCode.Location = new Point(270, 68);
            lblExamCode.Name = "lblExamCode";
            lblExamCode.Size = new Size(87, 20);
            lblExamCode.TabIndex = 11;
            lblExamCode.Text = "Exam Code:";
            // 
            // lblStudent
            // 
            lblStudent.AutoSize = true;
            lblStudent.Location = new Point(294, 22);
            lblStudent.Name = "lblStudent";
            lblStudent.Size = new Size(63, 20);
            lblStudent.TabIndex = 10;
            lblStudent.Text = "Student:";
            // 
            // lblTotalMark
            // 
            lblTotalMark.AutoSize = true;
            lblTotalMark.Location = new Point(25, 69);
            lblTotalMark.Name = "lblTotalMark";
            lblTotalMark.Size = new Size(114, 20);
            lblTotalMark.TabIndex = 9;
            lblTotalMark.Text = "Total Questions:";
            // 
            // lblDuration
            // 
            lblDuration.AutoSize = true;
            lblDuration.Location = new Point(69, 22);
            lblDuration.Name = "lblDuration";
            lblDuration.Size = new Size(70, 20);
            lblDuration.TabIndex = 8;
            lblDuration.Text = "Duration:";
            // 
            // lblDisplayTimeLeft
            // 
            lblDisplayTimeLeft.AutoSize = true;
            lblDisplayTimeLeft.Font = new Font("Segoe UI", 36F, FontStyle.Bold, GraphicsUnit.Point);
            lblDisplayTimeLeft.ForeColor = Color.Blue;
            lblDisplayTimeLeft.Location = new Point(693, 7);
            lblDisplayTimeLeft.Name = "lblDisplayTimeLeft";
            lblDisplayTimeLeft.Size = new Size(161, 81);
            lblDisplayTimeLeft.TabIndex = 17;
            lblDisplayTimeLeft.Text = "NaN";
            // 
            // lblTime
            // 
            lblTime.AutoSize = true;
            lblTime.Location = new Point(598, 67);
            lblTime.Name = "lblTime";
            lblTime.Size = new Size(71, 20);
            lblTime.TabIndex = 16;
            lblTime.Text = "Time left:";
            // 
            // btnFinish
            // 
            btnFinish.Enabled = false;
            btnFinish.Location = new Point(12, 511);
            btnFinish.Name = "btnFinish";
            btnFinish.Size = new Size(94, 29);
            btnFinish.TabIndex = 19;
            btnFinish.Text = "Finish";
            btnFinish.UseVisualStyleBackColor = true;
            // 
            // chkFinish
            // 
            chkFinish.AutoSize = true;
            chkFinish.Checked = true;
            chkFinish.CheckState = CheckState.Checked;
            chkFinish.Enabled = false;
            chkFinish.ForeColor = Color.Blue;
            chkFinish.Location = new Point(12, 468);
            chkFinish.Name = "chkFinish";
            chkFinish.Size = new Size(193, 24);
            chkFinish.TabIndex = 18;
            chkFinish.Text = "I want to finish the exam";
            chkFinish.UseVisualStyleBackColor = true;
            // 
            // lblMark
            // 
            lblMark.AutoSize = true;
            lblMark.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            lblMark.Location = new Point(147, 180);
            lblMark.Name = "lblMark";
            lblMark.Size = new Size(313, 140);
            lblMark.TabIndex = 20;
            lblMark.Text = "Your mark of this test is 10.\r\n\r\nThe score will be store in database.\r\n\r\n\r\n";
            // 
            // lblSubmitSuccess
            // 
            lblSubmitSuccess.AutoSize = true;
            lblSubmitSuccess.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            lblSubmitSuccess.ForeColor = Color.Blue;
            lblSubmitSuccess.Location = new Point(122, 511);
            lblSubmitSuccess.Name = "lblSubmitSuccess";
            lblSubmitSuccess.Size = new Size(262, 28);
            lblSubmitSuccess.TabIndex = 21;
            lblSubmitSuccess.Text = "Submit exam successfully!";
            // 
            // SubmitForm
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(987, 552);
            Controls.Add(lblSubmitSuccess);
            Controls.Add(lblMark);
            Controls.Add(btnFinish);
            Controls.Add(chkFinish);
            Controls.Add(lblDisplayTimeLeft);
            Controls.Add(lblTime);
            Controls.Add(lblDisplayStudent);
            Controls.Add(lblDisplayTotalMarks);
            Controls.Add(lblDisplayDuration);
            Controls.Add(lblDisplayExamCode);
            Controls.Add(lblExamCode);
            Controls.Add(lblStudent);
            Controls.Add(lblTotalMark);
            Controls.Add(lblDuration);
            Controls.Add(btnExit);
            Name = "SubmitForm";
            Text = "SubmitForm";
            FormClosing += SubmitForm_FormClosing;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button btnExit;
        private Label lblDisplayStudent;
        private Label lblDisplayTotalMarks;
        private Label lblDisplayDuration;
        private Label lblDisplayExamCode;
        private Label lblExamCode;
        private Label lblStudent;
        private Label lblTotalMark;
        private Label lblDuration;
        private Label lblDisplayTimeLeft;
        private Label lblTime;
        private Button btnFinish;
        private CheckBox chkFinish;
        private Label lblMark;
        private Label lblSubmitSuccess;
    }
}