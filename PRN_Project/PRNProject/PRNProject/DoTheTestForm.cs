﻿using PRNProject.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Rebar;

namespace PRNProject
{
    public partial class DoTheTestForm : Form
    {
        public DoTheTestForm()
        {
            InitializeComponent();
            this.Text = "EOS Platform";
        }
        public DoTheTestForm(string examCode, string studentName)
        {
            InitializeComponent();
            this.Text = "EOS Platform";
            using (WinformProjectContext context = new WinformProjectContext())
            {
                Student s = context.Students.FirstOrDefault(s => s.Account == studentName);
                lblDisplayStudent.Text = studentName;
                Exam e = context.Exams.FirstOrDefault(e => e.Code == examCode);
                lblDisplayExamCode.Text = e.SubjectName;
                lblDisplayDuration.Text = e.Time + " minutes";
                lblDisplayTotalMarks.Text = e.TotalMark + "";
                lblReportTotalMark.Text = "There are " + e.TotalMark + " questions, and your progress of answering is";
                SetInportantInput(e.Code, s.Account);

                comboSettingContent();
                SetTimer((int)e.Time, 1);
                StartTimer();
            }
        }

        private System.Windows.Forms.Timer countdownTimer;
        private int remainingMinutes = 0;
        private int remainingSeconds = 0;

        private string codeExam = "";
        private string studentName = "";

        private List<Question> ListQuestion = new List<Question>();
        private int orderOfQuestion = 0;
        private List<Answer> ListAnswerHasBeenChecked = new List<Answer>();

        // SETTING INPUT
        public void comboSettingContent()
        {
            setShowQuestion(ListQuestion[orderOfQuestion]);
            setShowAnswer(ListQuestion[orderOfQuestion]);
            createPanel(ListQuestion[orderOfQuestion]);
        }

        private void SetInportantInput(string code, string name)
        {
            Random rand = new Random();
            codeExam = code;
            studentName = name;
            using (WinformProjectContext context = new WinformProjectContext())
            {
                List<Question> questions = context.Questions.Where(q => q.ExamCode == code).ToList();

                for (int i = 0; i < questions.Count(); i++)
                {
                    int j = rand.Next(questions.Count());
                    Question temp = questions[i];
                    questions[i] = questions[j];
                    questions[j] = temp;
                }
                ListQuestion = questions;
            }
            orderOfQuestion = 0;
        }

        private void setShowQuestion(Question question)
        {
            lblShowQuestion.Text = question.Title;
        }

        private void setShowAnswer(Question question)
        {
            using (WinformProjectContext context = new WinformProjectContext())
            {
                List<Answer> answers = context.Answers.Where(answer => answer.QuestionCode == question.Code).ToList();
                int NumberOfCorrectAnswer = answers.Where(q => q.IsCorrect == true).ToList().Count();
                if (NumberOfCorrectAnswer == 1)
                {
                    lblShowChooseAnswer.Text = "(Choose 1 answer) \n\n";
                }
                else
                {
                    lblShowChooseAnswer.Text = "(Choose " + NumberOfCorrectAnswer + " answers)\n\n";
                }
                foreach (Answer answer in answers)
                {
                    lblShowChooseAnswer.Text += "\n" + answer.Title + "\n";
                }
            }
        }

        // SETTING UPDATE PROGRESS BAR
        private int GetNumberOfQuestionBeenAnswered()
        {
            List<int> questionBeenAnswered = new List<int>();
            ListAnswerHasBeenChecked = ListAnswerHasBeenChecked.Distinct().ToList();
            foreach (Answer answer in ListAnswerHasBeenChecked)
            {
                questionBeenAnswered.Add(answer.QuestionCode);
            }
            return questionBeenAnswered.Distinct().ToList().Count();
        }

        private void updateProgressBar()
        {
            int percent = (int)((double)GetNumberOfQuestionBeenAnswered() / ListQuestion.Count() * 100);
            pgbProcess.Value = percent;
        }

        // SETTING FONT & SIZE OF CONTENT
        private void numSize_ValueChanged(object sender, EventArgs e)
        {
            lblShowChooseAnswer.Font = new Font(lblShowChooseAnswer.Font.FontFamily,
                (float)numSize.Value);
            lblShowQuestion.Font = new Font(lblShowQuestion.Font.FontFamily,
                (float)numSize.Value);

        }

        private void domFont_SelectedItemChanged(object sender, EventArgs e)
        {
            lblShowChooseAnswer.Font = new Font(domFont.SelectedItem.ToString(),
                lblShowChooseAnswer.Font.Size);
            lblShowQuestion.Font = new Font(domFont.SelectedItem.ToString(),
                lblShowQuestion.Font.Size);
        }
        // SETTING TIME COUNT
        private void SetTimer(int minutes, int seconds)
        {
            remainingMinutes = minutes;
            remainingSeconds = seconds;
        }

        private void StartTimer()
        {
            countdownTimer = new System.Windows.Forms.Timer();
            countdownTimer.Interval = 1000; // 1 second
            countdownTimer.Tick += tmTimeLeft_Tick; // Attach the event handler
            countdownTimer.Start();
        }

        private void tmTimeLeft_Tick(object sender, EventArgs e)
        {
            // Decrement the remaining time
            if (remainingSeconds > 0)
            {
                remainingSeconds--;
            }
            else
            {
                if (remainingMinutes > 0)
                {
                    remainingMinutes--;
                    remainingSeconds = 59;
                }
                else
                {
                    // Countdown finished
                    countdownTimer.Stop();
                    MessageBox.Show("Time out!");
                    this.Hide();
                    storeTheMark();
                    SubmitForm submitForm = new SubmitForm(codeExam, studentName, lblDisplayTimeLeft.Text);
                    submitForm.Show();
                }
            }

            // Update the label
            UpdateTimeLabel();
        }

        private void UpdateTimeLabel()
        {
            // Format the remaining time as "mm:ss"
            string formattedTime = $"{remainingMinutes:D2}:{remainingSeconds:D2}";
            lblDisplayTimeLeft.Text = formattedTime;
        }

        // SETTING NEXT & BACK BUTTON

        private void btnBack_Click(object sender, EventArgs e)
        {
            deletePanel();
            orderOfQuestion = orderOfQuestion - 1;
            if (orderOfQuestion < 0)
            {
                orderOfQuestion = ListQuestion.Count() - 1;
            }
            comboSettingContent();
            updateProgressBar();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            deletePanel();
            orderOfQuestion = orderOfQuestion + 1;
            if (orderOfQuestion > ListQuestion.Count() - 1)
            {
                orderOfQuestion = 0;
            }
            comboSettingContent();
            updateProgressBar();
        }

        // SETTING CHECKBOX ANSWER
        public void createPanel(Question question)
        {
            using (WinformProjectContext context = new WinformProjectContext())
            {
                int numberOfAnswer = context.Answers.Where(a => a.QuestionCode == question.Code).ToList().Count();
                //Panel panel = new Panel();
                //panel.Name = "pnAnswers";
                //panel.Location = new Point(12, 250);
                //panel.Size = new Size(110, 320);

                for (int i = 0; i < numberOfAnswer; i++)
                {
                    CheckBox checkBox = new CheckBox();
                    checkBox.Text = Convert.ToChar(65 + i).ToString();
                    checkBox.Location = new Point(25, i * 60);
                    checkBox.Size = new Size(41, 24);
                    checkBox.Name = question.Code + "_" + (i + 1);
                    checkBox.Checked = updateAnswerBeenClickedToTheForm(question.Code, i + 1);
                    checkBox.CheckedChanged += CheckBoxAnswer_CheckedChanged;

                    pnAnswers.Controls.Add(checkBox);
                }
            }
        }

        public void deletePanel()
        {
            //Panel p = (Panel)this.Controls.Find("pnAnswers", true).FirstOrDefault();
            //if (p != null)
            //{
            //    this.Controls.Remove(p);
            //    p.Dispose();
            //}
            pnAnswers.Controls.Clear();
        }

        public void updateAnswerBeenClickedToTheList(string id, bool check)
        {
            using (WinformProjectContext ctx = new WinformProjectContext())
            {
                int questionCode = int.Parse(id.Split('_')[0]);
                int answerOrder = int.Parse(id.Split('_')[1]);
                Answer selectedAnswer = ctx.Answers.FirstOrDefault(a =>
                a.QuestionCode == questionCode && a.Order == answerOrder);

                bool checkExist = updateAnswerBeenClickedToTheForm(questionCode, answerOrder);
                if (checkExist)
                {
                    if (!check)
                    {
                        ListAnswerHasBeenChecked.Remove(ListAnswerHasBeenChecked.FirstOrDefault(
                            a => a.QuestionCode == questionCode && a.Order == answerOrder));
                    }
                }
                else
                {
                    if (check)
                    {
                        ListAnswerHasBeenChecked.Add(selectedAnswer);
                    }
                }
            }
        }

        public bool updateAnswerBeenClickedToTheForm(int questionCode, int answerOrder)
        {
            bool checkExist = false;
            foreach (Answer a in ListAnswerHasBeenChecked)
            {
                if (a.QuestionCode == questionCode && a.Order == answerOrder)
                {
                    checkExist = true;
                    break;
                }
            }

            return checkExist;
        }

        private void CheckBoxAnswer_CheckedChanged(object? sender, EventArgs e)
        {
            CheckBox chk = (CheckBox)sender;
            updateAnswerBeenClickedToTheList(chk.Name, chk.Checked);
        }

        // SETTING EXIT FORM
        private void DoTheTestForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
            }
        }

        private void chkFinish_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFinish.Checked == false)
            {
                btnFinish.Enabled = false;
            }
            else
            {
                btnFinish.Enabled = true;
            }
        }

        private void btnFinish_Click(object sender, EventArgs e)
        {
            countdownTimer.Stop();
            this.Hide();
            storeTheMark();
            SubmitForm submitForm = new SubmitForm(codeExam, studentName, lblDisplayTimeLeft.Text);
            submitForm.Show();
        }

        // CALCULATE & STORE THE MARK                

        private bool checkCorrectAnswer(int questionCode)
        {
            using (WinformProjectContext ctx = new WinformProjectContext())
            {
                List<Answer> answerBeenTickedList = ListAnswerHasBeenChecked.Where
                    (a => a.QuestionCode == questionCode).ToList();
                List<Answer> correctAnswer = ctx.Answers.Where
                    (a => a.QuestionCode == questionCode && a.IsCorrect == true).ToList();

                bool check = true;
                int numberOfCorrectAnswer = 0;

                foreach (Answer a in answerBeenTickedList)
                {
                    if (a.IsCorrect == false)
                    {
                        check = false;
                        break;
                    }
                    else
                    {
                        numberOfCorrectAnswer = numberOfCorrectAnswer + 1;
                    }
                }
                if (numberOfCorrectAnswer != correctAnswer.Count())
                {
                    check = false;
                }
                return check;
            }
        }

        private string calculateTheMark()
        {
            using (WinformProjectContext context = new WinformProjectContext())
            {
                double mark = 0;
                foreach (Question q in ListQuestion)
                {
                    if (checkCorrectAnswer(q.Code))
                    {
                        mark += (double)10 / ListQuestion.Count();
                    }
                }
                string formattedMark;
                if (Math.Floor(mark) == mark)
                {
                    formattedMark = mark.ToString("0");
                }
                else
                {
                    formattedMark = mark.ToString("N2");
                }
                return formattedMark;
            }
        }

        private void storeTheMark()
        {
            using (WinformProjectContext ctx = new WinformProjectContext())
            {
                double mark = double.Parse(calculateTheMark());
                Mark m = new Mark()
                {
                    Mark1 = mark,
                    StudentAccount = studentName,
                    ExamCode = codeExam
                };
                ctx.Marks.Add(m);
                ctx.SaveChanges();
            }
        }
    }
}
