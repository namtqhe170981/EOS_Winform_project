﻿namespace PRNProject
{
    partial class ResponseAssigned
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            dtReport = new DataGridView();
            btnApprove = new Button();
            btnExit = new Button();
            ((System.ComponentModel.ISupportInitialize)dtReport).BeginInit();
            SuspendLayout();
            // 
            // dtReport
            // 
            dtReport.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dtReport.Location = new Point(167, 50);
            dtReport.Name = "dtReport";
            dtReport.RowHeadersWidth = 51;
            dtReport.RowTemplate.Height = 29;
            dtReport.Size = new Size(485, 243);
            dtReport.TabIndex = 0;
            dtReport.CellContentClick += dtReport_CellContentClick;
            // 
            // btnApprove
            // 
            btnApprove.Location = new Point(304, 363);
            btnApprove.Name = "btnApprove";
            btnApprove.Size = new Size(94, 29);
            btnApprove.TabIndex = 1;
            btnApprove.Text = "Approve";
            btnApprove.UseVisualStyleBackColor = true;
            btnApprove.Click += btnApprove_Click;
            // 
            // btnExit
            // 
            btnExit.Location = new Point(469, 363);
            btnExit.Name = "btnExit";
            btnExit.Size = new Size(94, 29);
            btnExit.TabIndex = 2;
            btnExit.Text = "Exit";
            btnExit.UseVisualStyleBackColor = true;
            btnExit.Click += btnExit_Click;
            // 
            // ResponseAssigned
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(btnExit);
            Controls.Add(btnApprove);
            Controls.Add(dtReport);
            Name = "ResponseAssigned";
            Text = "ResponseAssigned";
            Load += ResponseAssigned_Load;
            ((System.ComponentModel.ISupportInitialize)dtReport).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private DataGridView dtReport;
        private Button btnApprove;
        private Button btnExit;
    }
}