﻿namespace PRNProject
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            lblExam = new Label();
            lblAccount = new Label();
            lblPass = new Label();
            txtExam = new TextBox();
            txtAccount = new TextBox();
            txtPass = new TextBox();
            btnLogin = new Button();
            btnExit = new Button();
            label1 = new Label();
            lblDomain = new Label();
            txtDomain = new TextBox();
            SuspendLayout();
            // 
            // lblExam
            // 
            lblExam.AutoSize = true;
            lblExam.Location = new Point(144, 37);
            lblExam.Name = "lblExam";
            lblExam.Size = new Size(82, 20);
            lblExam.TabIndex = 0;
            lblExam.Text = "Exam code";
            // 
            // lblAccount
            // 
            lblAccount.AutoSize = true;
            lblAccount.Location = new Point(144, 134);
            lblAccount.Name = "lblAccount";
            lblAccount.Size = new Size(79, 20);
            lblAccount.TabIndex = 1;
            lblAccount.Text = "User name";
            // 
            // lblPass
            // 
            lblPass.AutoSize = true;
            lblPass.Location = new Point(144, 227);
            lblPass.Name = "lblPass";
            lblPass.Size = new Size(70, 20);
            lblPass.TabIndex = 2;
            lblPass.Text = "Password";
            // 
            // txtExam
            // 
            txtExam.Location = new Point(329, 37);
            txtExam.Name = "txtExam";
            txtExam.Size = new Size(347, 27);
            txtExam.TabIndex = 3;
            // 
            // txtAccount
            // 
            txtAccount.Location = new Point(329, 134);
            txtAccount.Name = "txtAccount";
            txtAccount.Size = new Size(347, 27);
            txtAccount.TabIndex = 4;
            // 
            // txtPass
            // 
            txtPass.Location = new Point(329, 227);
            txtPass.Name = "txtPass";
            txtPass.PasswordChar = '*';
            txtPass.Size = new Size(347, 27);
            txtPass.TabIndex = 5;
            // 
            // btnLogin
            // 
            btnLogin.Location = new Point(277, 371);
            btnLogin.Name = "btnLogin";
            btnLogin.Size = new Size(94, 29);
            btnLogin.TabIndex = 6;
            btnLogin.Text = "Login";
            btnLogin.UseVisualStyleBackColor = true;
            btnLogin.Click += btnLogin_Click;
            // 
            // btnExit
            // 
            btnExit.Location = new Point(466, 371);
            btnExit.Name = "btnExit";
            btnExit.Size = new Size(94, 29);
            btnExit.TabIndex = 7;
            btnExit.Text = "Exit";
            btnExit.UseVisualStyleBackColor = true;
            btnExit.Click += btnExit_Click;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            label1.ForeColor = Color.Blue;
            label1.Location = new Point(233, 403);
            label1.Name = "label1";
            label1.Size = new Size(405, 28);
            label1.TabIndex = 8;
            label1.Text = "Register the exam may take time, please wait!";
            // 
            // lblDomain
            // 
            lblDomain.AutoSize = true;
            lblDomain.Location = new Point(144, 319);
            lblDomain.Name = "lblDomain";
            lblDomain.Size = new Size(62, 20);
            lblDomain.TabIndex = 9;
            lblDomain.Text = "Domain";
            // 
            // txtDomain
            // 
            txtDomain.Enabled = false;
            txtDomain.Location = new Point(329, 319);
            txtDomain.Name = "txtDomain";
            txtDomain.Size = new Size(347, 27);
            txtDomain.TabIndex = 10;
            // 
            // LoginForm
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(txtDomain);
            Controls.Add(lblDomain);
            Controls.Add(label1);
            Controls.Add(btnExit);
            Controls.Add(btnLogin);
            Controls.Add(txtPass);
            Controls.Add(txtAccount);
            Controls.Add(txtExam);
            Controls.Add(lblPass);
            Controls.Add(lblAccount);
            Controls.Add(lblExam);
            Name = "LoginForm";
            Text = "LoginForm";
            FormClosing += LoginForm_FormClosing;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label lblExam;
        private Label lblAccount;
        private Label lblPass;
        private TextBox txtExam;
        private TextBox txtAccount;
        private TextBox txtPass;
        private Button btnLogin;
        private Button btnExit;
        private Label label1;
        private Label lblDomain;
        private TextBox txtDomain;
    }
}